package regexp

import (
	"regexp"
	"strconv"
)

// Regexp is just a wrapper over the stdlib Regexp, but with some friendlier functions
type Regexp struct {
	*regexp.Regexp
}

// Compile parses a regular expression and returns, if successful,
// a Regexp object that can be used to match against text.
//
// When matching against text, the regexp returns a match that
// begins as early as possible in the input (leftmost), and among those
// it chooses the one that a backtracking search would have found first.
// This so-called leftmost-first matching is the same semantics
// that Perl, Python, and other implementations use, although this
// package implements it without the expense of backtracking.
func Compile(expr string) (*Regexp, error) {
	re, err := regexp.Compile(expr)
	if err != nil {
		return nil, err
	}
	return &Regexp{Regexp: re}, nil
}

// MustCompile is like Compile but panics if the expression cannot be parsed.
// It simplifies safe initialization of global variables holding compiled regular
// expressions.
func MustCompile(expr string) *Regexp {
	re, err := Compile(expr)
	if err != nil {
		panic(err)
	}
	return re
}

// Groups is a map of group indexes/names to their match
type Groups map[string]string

// Index gets a group by its index
// 0 is the full match
// 1 is the first group match
// 2 is the second group match
// ...
// If the index doesn't exist, a blank string is returned
func (g Groups) Index(idx int) string {
	return g[strconv.Itoa(idx)]
}

// Name gets a group by its name if set
// If the name doesn't exist, a blank string is returned
func (g Groups) Name(name string) string {
	return g[name]
}

// Groups combines regexp's SubexpNames and FindStringSubmatch
// funcs to create a mapping of indexes/names for a single match
func (re *Regexp) Groups(s string) Groups {
	groups := make(map[string]string)
	names := re.SubexpNames()
	for idx, match := range re.FindStringSubmatch(s) {
		groups[strconv.Itoa(idx)] = match
		if names[idx] != "" {
			groups[names[idx]] = match
		}
	}
	return groups
}

// AllGroups is the All version of Groups
func (re *Regexp) AllGroups(s string) []Groups {
	allGroups := make([]Groups, 0, 5)
	names := re.SubexpNames()
	for _, match := range re.FindAllStringSubmatch(s, -1) {
		groups := make(map[string]string)
		for idy, sub := range match {
			groups[strconv.Itoa(idy)] = sub
			if names[idy] != "" {
				groups[names[idy]] = sub
			}
		}
		allGroups = append(allGroups, groups)
	}
	return allGroups
}
