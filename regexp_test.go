package regexp

import (
	"fmt"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestGroups(t *testing.T) {
	re := MustCompile(`(?P<first>[a-zA-Z]+) ([a-zA-Z]+) (?P<last>[a-zA-Z]+)`)

	tt := []struct {
		Name    string
		Expr    string
		Indexes map[int]string
		Names   map[string]string
	}{
		{
			Name: "Go Example",
			Expr: "Alan Turing",
			Indexes: map[int]string{
				0:  "",
				3:  "",
				99: "",
			},
			Names: map[string]string{
				"first":  "",
				"second": "",
				"last":   "",
			},
		},
		{
			Name: "Go Example Correct",
			Expr: "Alan Mathison Turing",
			Indexes: map[int]string{
				0: "Alan Mathison Turing",
				1: "Alan",
				2: "Mathison",
				3: "Turing",
				4: "",
			},
			Names: map[string]string{
				"first":  "Alan",
				"second": "",
				"last":   "Turing",
			},
		},
		{
			Name: "Four words",
			Expr: "This is a test",
			Indexes: map[int]string{
				0: "This is a",
				1: "This",
				2: "is",
				3: "a",
				4: "",
			},
			Names: map[string]string{
				"first":  "This",
				"second": "",
				"last":   "a",
			},
		},
		{
			Name: "Two matches",
			Expr: "This is a test of regex",
			Indexes: map[int]string{
				0: "This is a",
				1: "This",
				2: "is",
				3: "a",
				4: "",
			},
			Names: map[string]string{
				"first":  "This",
				"second": "",
				"last":   "a",
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			groups := re.Groups(tc.Expr)
			for idx, res := range tc.Indexes {
				if groups.Index(idx) != res {
					t.Logf("\ngot:\t%s\nexpected:\t%s", groups.Index(idx), res)
					t.Fail()
				}
			}
			for idx, res := range tc.Names {
				if groups.Name(idx) != res {
					t.Logf("\ngot:\t%s\nexpected:\t%s", groups.Name(idx), res)
					t.Fail()
				}
			}
		})
	}
}

func TestAllGroups(t *testing.T) {
	re := MustCompile(`(?P<first>[a-zA-Z]+) ([a-zA-Z]+) (?P<last>[a-zA-Z]+)`)

	tt := []struct {
		Name    string
		Expr    string
		Indexes []map[int]string
		Names   []map[string]string
	}{
		{
			Name: "Two matches",
			Expr: "This is a test of regex",
			Indexes: []map[int]string{
				{
					0: "This is a",
					1: "This",
					2: "is",
					3: "a",
					4: "",
				},
				{
					0: "test of regex",
					1: "test",
					2: "of",
					3: "regex",
					4: "",
				},
			},
			Names: []map[string]string{
				{
					"first":  "This",
					"second": "",
					"last":   "a",
				},
				{
					"first":  "test",
					"second": "",
					"last":   "regex",
				},
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.Name, func(t *testing.T) {
			for idx, groups := range re.AllGroups(tc.Expr) {
				for idy, res := range tc.Indexes[idx] {
					if groups.Index(idy) != res {
						t.Logf("\ngot:\t%s\nexpected:\t%s", groups.Index(idy), res)
						t.Fail()
					}
				}
				for idy, res := range tc.Names[idx] {
					if groups.Name(idy) != res {
						t.Logf("\ngot:\t%s\nexpected:\t%s", groups.Name(idy), res)
						t.Fail()
					}
				}
			}
		})
	}
}

func ExampleGroups() {
	re := MustCompile(`articles/(?P<date>\d{4}/\d{2}/\d{2})/(?P<slug>[-a-zA-Z0-9]*)%(?P<lang>[_a-zA-Z]{2,5})\.rst`)
	groups := re.Groups("articles/2020/04/02/how-to-write-regex%en.rst")
	fmt.Println("date", groups.Name("date"))
	fmt.Println("slug", groups.Name("slug"))
	fmt.Println("lang", groups.Name("lang"))
	// Output: date 2020/04/02
	// slug how-to-write-regex
	// lang en
}
