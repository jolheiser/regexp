# regexp

This package is just a small extension to the stdlib to make groups easier to work with.  
It should be drop-in compatible with the stdlib regexp. Any package-level functions are replicated and delegated to the stdlib. 

Take this example I copied from a site explaining how to do it in stdlib

### stdlib regexp
```go
package main

import (
        "fmt"
        "regexp"
)

var example = "articles/2020/04/02/how-to-write-regex%en.rst"

func main() {
        pattern := `articles/(?P<date>\d{4}/\d{2}/\d{2})/(?P<slug>[-a-zA-Z0-9]*)%(?P<lang>[_a-zA-Z]{2,5})\.rst`
        pathMetadata := regexp.MustCompile(pattern)

        matches := pathMetadata.FindStringSubmatch(example)
        names := pathMetadata.SubexpNames()
        for i, match := range matches {
                if i != 0 {
                        fmt.Println(names[i], match)
                }
        }
}
```

### go.jolheiser.com/regexp
```go
package main

import (
        "fmt"

        "go.jolheiser.com/regexp"
)

var example = "articles/2020/04/02/how-to-write-regex%en.rst"

func main() {
        pattern := `articles/(?P<date>\d{4}/\d{2}/\d{2})/(?P<slug>[-a-zA-Z0-9]*)%(?P<lang>[_a-zA-Z]{2,5})\.rst`
        pathMetadata := regexp.MustCompile(pattern)

        groups := pathMetadata.Groups(example)
        fmt.Println("date", groups.Name("date"))
        fmt.Println("slug", groups.Name("slug"))
        fmt.Println("lang", groups.Name("lang"))
}
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for the full text.